# Filebot

This bot connects to two network shares and simulates some basic user actions. One of the shares needs authentication over username/password. The other share doesn't need an authentication.